<?php

/**
 * Fired during plugin activation
 *
 * @link       https://flauntyoursite.com
 * @since      1.0.0
 *
 * @package    Ppsdc_Core
 * @subpackage Ppsdc_Core/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Ppsdc_Core
 * @subpackage Ppsdc_Core/includes
 * @author     William Bay <william@flauntyoursite.com>
 */
class Ppsdc_Core_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
